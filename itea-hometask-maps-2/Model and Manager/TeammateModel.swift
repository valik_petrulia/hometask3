//
//  TeammateModel.swift
//  itea-hometask-maps-2
//
//  Created by Валентин Петруля on 7/1/19.
//  Copyright © 2019 Валентин Петруля. All rights reserved.
//

import Foundation

class Teammate {
    var avatar: String
    var name: String
    var surname: String
    var address: String
    var email: String
    var phone: String
    var longitude: Double
    var latitude: Double
    
    init(avatar: String, name: String, surname: String, address: String, email: String, phone: String, latitude: Double, longitude: Double) {
        self.avatar = avatar
        self.name = name
        self.surname = surname
        self.address = address
        self.email = email
        self.phone = phone
        self.longitude = longitude
        self.latitude = latitude
    }
}
