//
//  DetailViewController.swift
//  itea-hometask-maps-2
//
//  Created by Валентин Петруля on 7/7/19.
//  Copyright © 2019 Валентин Петруля. All rights reserved.
//

import UIKit

protocol InfoViewDelegate {
    func viewSendMessage(to teammate: Teammate)
    func viewSendMail(to teammate: Teammate)
}

class DetailViewController: UIViewController {
    
    @IBOutlet weak var avatarImageView: UIImageView!
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var surnameLabel: UILabel!
    @IBOutlet weak var addressLabel: UILabel!
    @IBOutlet weak var emailLabel: UILabel!
    @IBOutlet weak var phoneLabel: UILabel!
    
    var teammate: Teammate!
    
    var delegate: InfoViewDelegate?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationController?.navigationBar.isHidden = true
        avatarImageView.image = UIImage(named: teammate.avatar)
        nameLabel.text = teammate.name
        surnameLabel.text = teammate.surname
        addressLabel.text = teammate.address
        emailLabel.text = teammate.email
        phoneLabel.text = teammate.phone
    }

    @IBAction func didTapMailButton(_ sender: Any) {
        delegate?.viewSendMail(to: teammate)
    }
    
    @IBAction func didTapMessageButton(_ sender: Any) {
        delegate?.viewSendMessage(to: teammate)
    }
    
    @IBAction func didTapCloseButton(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
}
