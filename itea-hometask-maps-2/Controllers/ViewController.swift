//
//  ViewController.swift
//  itea-hometask-maps-2
//
//  Created by Валентин Петруля on 7/1/19.
//  Copyright © 2019 Валентин Петруля. All rights reserved.
//

import UIKit
import GoogleMaps
import GooglePlaces
import MessageUI

class ViewController: UIViewController {
    
    
    @IBOutlet weak var streetLabel: UILabel!
    @IBOutlet weak var mapView: GMSMapView!
    
    let teammates = TeammateManager()
    var locationManager = CLLocationManager()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setCurrentLocation()
        setMarkers()
        streetLabel.text = ""
    }
}

extension ViewController: CLLocationManagerDelegate, GMSMapViewDelegate {
    func setCurrentLocation() {
        mapView.delegate = self
        mapView.isMyLocationEnabled = true
        locationManager.delegate = self
        locationManager.startUpdatingLocation()
    }
    
    
    func setMarkers() {
        for teammate in teammates.array {
            let marker = GMSMarker()
            let markerImage = UIImageView(frame: CGRect(x: 0, y: 0, width: 40, height: 40))
            markerImage.image = UIImage(named: "marker")
            marker.iconView = markerImage
            marker.position = CLLocationCoordinate2D(latitude: teammate.latitude, longitude: teammate.longitude)
            marker.title = ""
            marker.map = mapView
        }
    }
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        let camera = GMSCameraPosition.camera(withLatitude: CLLocationDegrees(50.4163883), longitude: CLLocationDegrees(30.4083956), zoom: 11.5)
        self.mapView.animate(to: camera)
        self.locationManager.stopUpdatingLocation()
    }
    
    func mapView(_ mapView: GMSMapView, markerInfoWindow marker: GMSMarker) -> UIView? {
        let alert = UIAlertController(title: "Внимание!", message: "Какую страничку отобразить?", preferredStyle: .alert)
        var customView = UIView()
        alert.addAction(UIAlertAction(title: "Краткую", style: .default, handler: { (action) in
            alert.dismiss(animated: true, completion: nil)
            if let markerView = Bundle.main.loadNibNamed("MarkerInfoView", owner: self, options: nil)?[0] as? MarkerInfoView {
                for teammate in self.teammates.array {
                    if marker.position.latitude == teammate.latitude && marker.position.longitude == teammate.longitude {
                        markerView.avatarImageView.image = UIImage(named: teammate.avatar)
                        markerView.nameLabel.text = teammate.name
                        markerView.surnameLabel.text = teammate.surname
                        markerView.addressLabel.text = teammate.address
                        markerView.phoneLabel.text = teammate.phone
                        self.streetLabel.text = teammate.address
                    }
                }
                markerView.avatarImageView.layer.cornerRadius = markerView.avatarImageView.frame.size.width / 2
                markerView.avatarImageView.clipsToBounds = true
                markerView.contentInfoView.layer.cornerRadius = 10
                markerView.contentInfoView.layer.borderColor = UIColor.black.cgColor
                markerView.contentInfoView.layer.borderWidth = 1
                customView = markerView
                self.view.addSubview(customView)
                
            }
        }))
        
        alert.addAction(UIAlertAction(title: "Полную", style: .default, handler: { (action) in
            alert.dismiss(animated: true, completion: nil)
            let vc = self.storyboard?.instantiateViewController(withIdentifier: "DetailViewController") as! DetailViewController
            for teammate in self.teammates.array {
                if marker.position.latitude == teammate.latitude && marker.position.longitude == teammate.longitude {
                    vc.teammate = teammate
                }
            }
            self.navigationController?.pushViewController(vc, animated: true)
        }))
        self.present(alert, animated: true, completion: nil)
        return customView
        
//        var view: UIView?
//        if let markerView = Bundle.main.loadNibNamed("MarkerInfoView", owner: self, options: nil)?[0] as? MarkerInfoView {
//            for teammate in teammates.array {
//                if marker.position.latitude == teammate.latitude && marker.position.longitude == teammate.longitude {
//                    markerView.avatarImageView.image = UIImage(named: teammate.avatar)
//                    markerView.nameLabel.text = teammate.name
//                    markerView.surnameLabel.text = teammate.surname
//                    markerView.addressLabel.text = teammate.address
//                    markerView.phoneLabel.text = teammate.phone
//                    streetLabel.text = teammate.address
//                    view = markerView
//                }
//            }
//            markerView.avatarImageView.layer.cornerRadius = markerView.avatarImageView.frame.size.width / 2
//            markerView.avatarImageView.clipsToBounds = true
//            markerView.contentInfoView.layer.cornerRadius = 10
//            markerView.contentInfoView.layer.borderColor = UIColor.black.cgColor
//            markerView.contentInfoView.layer.borderWidth = 1
//            //return markerView
//        } else {
//            view = UIView()
//        }
//        return view
    }
}

extension ViewController: InfoViewDelegate {
    func viewSendMessage(to teammate: Teammate) {
        let messageCompose = MFMessageComposeViewController()
        messageCompose.messageComposeDelegate = (self as! MFMessageComposeViewControllerDelegate)
        messageCompose.recipients = [teammate.phone]
        messageCompose.body = "Hello, teacher"
        messageCompose.subject = "From students"
        present(messageCompose, animated: true, completion: nil)
    }
    
    func viewSendMail(to teammate: Teammate) {
        let composeVC = MFMailComposeViewController()
        composeVC.mailComposeDelegate = (self as! MFMailComposeViewControllerDelegate)
        composeVC.setToRecipients([teammate.email])
        composeVC.setSubject("Hello ITEA")
        composeVC.setMessageBody("SOMETHING MUST BE HERE", isHTML: false)
        present(composeVC, animated: true, completion: nil)
    }

}
